# Write a program which will find all such numbers which are divisible by 7 but are not divisible by 5,
# between 2000 and 3200 (inclusive).
# The numbers obtained should be printed in a comma-separated sequence on a single line.

def filter_numbers(numbers):
    return filter(lambda x: x % 7 == 0 and x % 5 != 0, numbers)

if __name__ == '__main__':
    numbers = range(2000, 3201)
    print filter_numbers(numbers)