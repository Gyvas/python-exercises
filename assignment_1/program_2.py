# Write a program which can compute the factorial of a given number.
# Suppose the following input is supplied to the program:
# 8
# Then, the output should be:
# 40320
#
# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.

def factorial(n):
    if n != 0:
        return n * factorial(n - 1)
    else:
        return 1

# Avoids imports
if __name__ == '__main__':
    number = int(raw_input('Input number for factorial: '))
    if number >= 0:
        print factorial(number)
    else:
        print 'Number for factorial must be a positive integer.'