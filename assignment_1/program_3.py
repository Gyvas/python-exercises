# Write a program which accepts a sequence of comma-separated numbers from console and generate a list and a tuple which contains every number.
# Suppose the following input is supplied to the program:
# 34,67,55,33,12,98
# Then, the output should be:
# ['34', '67', '55', '33', '12', '98']
# ('34', '67', '55', '33', '12', '98')

if __name__ == '__main__':
    input_string = raw_input('Input numbers: ')

    lst = list(input_string.split(','))
    lst = map(int, lst)
    tpl = tuple(lst)

    print lst
    print tpl