# Define a class which has at least two methods:
# getString: to get a string from console input
# printString: to print the string in upper case.

class ManageStrings(object):
    def __init__(self):
        self.data = ''

    def get_string(self):
        self.data = raw_input('Input your string: ')

    def print_string(self):
        print 'Your string is:', self.data.upper()

if __name__ == '__main__':
    example = ManageStrings()
    example.get_string()
    example.print_string()