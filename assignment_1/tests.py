# Write tests for class methods. You can run them using nosetests

from program_1 import filter_numbers
from program_2 import factorial

def test_factorial():
    assert factorial(8) == 40320

def test_print_numbers():
    assert filter_numbers(range(1, 36)) == [7, 14, 21, 28]

