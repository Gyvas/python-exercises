# Write a program which takes 2 digits, X,Y as input and generates a 2-dimensional array. The element value in the i-th row and j-th column of the array should be i*j.
# Example
# Suppose the following inputs are given to the program:
# 3,5
# Then, the output of the program should be:
# [[0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4, 6, 8]]
#
# Hints:
# Note: In case of input data being supplied to the question, it should be assumed to be a console input in a comma-separated form.

def generate_array(row, column):
    return [[i*j for j in range(column)] for i in range(row)]

def string_to_int_list(string):
    string = list(string.split(','))
    return map(int, string)

if __name__ == '__main__':
    dimensions = raw_input('Input dimensions: ')
    dimensions = string_to_int_list(dimensions)
    print generate_array(dimensions[0], dimensions[1])