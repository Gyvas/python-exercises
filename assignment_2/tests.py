from program_1 import generate_array
from program_1 import string_to_int_list
from program_2 import square_list
from program_3 import remove_duplicates
from program_3 import sort_list

def test_generate_array():
    assert generate_array(3,5) == [[0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4, 6, 8]]

def test_strin_to_int_list():
    assert string_to_int_list('3,5') == [3, 5]

def test_square_list():
    assert square_list([5,6,1,2]) == [25, 36, 1, 4]

def test_remove_duplicates():
    assert remove_duplicates(['dog', '2', 'dog', 'cat', 'cat', 'cat']) == ['2', 'dog', 'cat']

def test_sort_true():
    assert sort_list(['b', 'c', 'a', 'd'], True) == ['d', 'c', 'b', 'a']

def test_sort_false():
    assert sort_list(['b', 'c', 'a', 'd']) == ['a', 'b', 'c', 'd']