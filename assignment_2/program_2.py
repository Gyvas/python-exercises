# Write a program which can map() to make a list whose elements are square of numbers between 1 and 20 (both included).
#
# Hints:
# Use map() to generate a list.
# Use lambda to define anonymous functions.

def square_list(lst):
    return list(map(lambda x: x**2, lst))

if __name__ == '__main__':
    data = range(1, 21)
    squared = square_list(data)
    print squared