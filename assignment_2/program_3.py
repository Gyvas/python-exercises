# Write a program that accepts a sequence of whitespace separated words as input and prints the words after removing all duplicate words and sorting them alphanumerically.
# Suppose the following input is supplied to the program:
# hello world and practice makes perfect and hello world again
# Then, the output should be:
# again and hello makes perfect practice world
# Add an extra kwarg 'reverse', if it is True, then the list should be sorted in reverse, if it's False or not passed, then the above functionality applies.
#
# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.
# We use set container to remove duplicated data automatically and then use sorted() to sort the data.

def remove_duplicates(lst):
    return list(set(lst))

def sort_list(lst, reverse=False):
    return sorted(lst, reverse=reverse)

if __name__ == '__main__':
    input_string = raw_input('Input words: ')
    lst = list(input_string.split(' '))

    lst = remove_duplicates(lst)
    lst = sort_list(lst)
    print lst